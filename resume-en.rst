Péricles Lopes Machado
========================

:Email: pericles.raskolnikoff@gmail.com
:Location: Porto Alegre/RS, Brazil
:Birth date: 02/02/1989


About
~~~~~

My formation is in distributed systems and physics simulation. Furthermore, during my undergraduation I participated of three IBM/ACM ICPC national finals (south america/Brazil).

I have a Computer engineering's bachelor degree and a Electric engineering's master degree from UFPA (Universidade Federal do Pará - Federal University of Pará).

Since 2008, I'm working with project and development of GUI's, distributed programming in C++, algorithms project and compiler development. And, since 2013, I work with Python.

Also, I work with commercial systems development using PHP and javascript, since 2011.  

Challenges are my fun, therefore I constantly participate of open-source projects and online contests like TopCoder, HackerRank, Google Code Jam, Challenge 24, etc.


_______



Some relevant experiences
~~~~~~~~~~~~~~~~~~~~~~~~~


Researcher at LPM/UFRGS, *2013* - today
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


AR2GEMS
_______
  Plugins development and improvements in the SGeMS software (`sgems`_). This project is financed by PETROBRAS.
  
  The system is developed in C++ and python.



Software engineer, X-Manager, *2012* - today
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


X-Manager - a *start-up* project
________________________________
  In X-Manager project (`xmanager`_), I'm developing a configurable business game and a professional social network. The objective of this project is offer a gamefied enviroment to trainning and recruitment of  talents in resources management.
  
  
  The system is written in Python, PHP and Javascript, The platform is completly configurable, reducing the construction cost of  new simulations environments.


  Currently, this project is in start-up phase at CEI/UFRGS (http://www.inf.ufrgs.br/cei/)
  


Researcher and software engineer, LANE (Laboratório de Análise Numérica em Eletro-magnetismo)/UFPA, *2008* - *2012*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

LANE SAGS - Simulator for grounding analysis
___________________________________________
  I worked with improvement and support. I developed a new QT4 GUI and I trained new users.
  
  During this project, I learned many concepts about distributed systems (MPI), openMP, pthreads, QT multi-thread environment, networking programming, C++.
  

YAEL - A language for simulation scenarios description
______________________________________________________
  During my undergraduation, I developed for LANE SAGS, a language for simulation scenarios description.
  
  This language was developed in C++, using Bison and Flex.


LANE MAXWELL - *meshless* simulator for electro-magnetics wave propagation
__________________________________________________________________________
  During my master degree, I worked with electro-magnetics wave propagation, using a *meshless* maxwell equation discretization (RPIM - Radial Point Interpolation Method)
  
  This system was developed in C++ and have a QT4 GUI. Currently, this software is a open source project called GoGoRPIM (`lanemaxwell`_).


Open Software Experience
~~~~~~~~~~~~~~~~~~~~~~~~

PyPacMan
________
  A clone of the classic game PacMan, developed in python, it's only a educational project that I used to study some python concepts about multi-thread programming.


Clever Language - Implementation of multi-thread support and others features (FFI, ncurses, etc.) 
_________________________________________________________________________________________________
  I designed and developed the Clever parallellism model. Furthermore, I work in improvement of  many features in the language.
  
  The Clever is developed by a member of PHP "core" team, Felipe Pena. 
  
  


gogoSokoban - A educational project, where I work with a AI development to solve sokobans
_________________________________________________________________________________________
  In this project, I developed a system using C++ that solve sokobans with A* and heuristics to find the best solutions.



Envents and contests
~~~~~~~~~~~~~~~~~~~~


South America/Brazil finalist at ACM ICPC  2008, 2009 and 2010 
______________________________________________________________
  With the GoGo40 team, I attended three consecutives ACM ICPC south america finals.
  

Golden medal at SBM/MEC OBMEP 2005 (Brazilian Public School Math Olympiads 2005 promoted by Brazilian Math Society and Education Ministery)
___________________________________________________________________________________________________________________________________________
  During my high school, I gained a golden medal at OBMEP 2005.



===============

Languages
~~~~~~~~~
- Portuguese (*Native language*): understand well, speak well, read well, write well
- English: understand reasonably, speak reasonably, read well, write reasonably

Abilities
~~~~~~~~~

Software Development
____________________
- Medium level in Python development 
- Advanced level in C/C++ development 
- Medium level in parallellism and concurrency (threads, mutexes, semaphores, condition variable)
- Medium level in distributed systems
- Programming knowledges in: Python, C, C++, Javascript, PHP, Lua, etc.
- Tools: Git, QMake, CMake, MySQL, Bison, Flex, MPI, Pthread etc.
- Platforms: Linux (Debian, Ubuntu),  Windows (XP, 7).
- Libraries: Qt, Readline, Zlib, GSL, GLUT, opengl, ogre, GMP etc.
- Medium level in physics simulators development


Major Influences
~~~~~~~~~~~~~~~~
Andrew S. Tanenbaum, Djisktra, Donald Knuth, Richard Bellman, Bjarne Stroustrup


Technical Publications
~~~~~~~~~~~~~~~~~~~~~~
- `Analysis of voltages induced on power outlets due to atmospheric discharges on Radio Base Stations`_, Elsevier
- `An automatic methodology for obtaining optimum shape factors for the radial point interpolation method`_, Journal of Microwaves and Optoelectronics

.. _`Analysis of voltages induced on power outlets due to atmospheric discharges on Radio Base Stations`: http://www.sciencedirect.com/science/article/pii/S0307904X13000346
.. _`An automatic methodology for obtaining optimum shape factors for the radial point interpolation method`: http://www.scielo.br/scielo.php?pid=S2179-10742011000200009&script=sci_arttext


Projects and links
~~~~~~~~~~~~~~~~~~
- `sgems`_: SGeMS public repository 
- `site`_: My personal site
- `brspoj`_: My SPOJ Brasil profile
- `sourceforge`_ : A personal open-source repository
- `github`_ : My personal profile at GitHub
- This `Currículo`_: `Repositório`_ 
- `Currículo Linked-in`_: Complete CV
- `xmanager`_:A Configurable business game

.. _`sgems`: https://github.com/ar2tech/ar2tech-SGeMS-public
.. _`site` : http://gogo40.com
.. _`sourceforge`: https://sourceforge.net/users/periclesmachado
.. _`github`: https://github.com/gogo40
.. _`Currículo`: https://github.com/gogo40/resume/blob/master/resume-pt_br.rst
.. _`Repositório`: https://github.com/gogo40/resume
.. _`Currículo Linked-in`: http://www.linkedin.com/profile/view?id=91897412
.. _`Sistema de gestão hospitalar GeHos`: periclesmachado.com/cliente/fernando_marques/gehos1.0
.. _`xmanager`: https://xmanager.co/
.. _`ssg_xplane_plugins`: https://github.com/gogo40/ssg_xplane_plugins
.. _`brspoj`: http://br.spoj.pl/users/gogo40
.. _`lanemaxwell`: https://github.com/gogo40/GoGoRPIM


